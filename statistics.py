"""Application entry point."""

import time
from typing import List, Tuple

from numpy import linspace

from localization.localization_service import LocalizationService, Result, LocalizationServiceActors, LocalizationServiceSettings
from localization.locator import Locator
from localization.observed_item import SimulatedObservedItem, ReplayObservedItem
from localization.base_actor import BaseActor
from localization.math_utils import Point
from localization.messages import Start
from localization.view import TextView

NUMBER_OF_MEASUREMENTS: int = 71


def main() -> None:
    """Simulation entry point."""

    bounding_box = ((0, 0), (500, 500))

    position_1: Point = (0, 21)
    position_2: Point = (60, 21)
    positions: List[Point] = [position_1, position_2]

    settings_1 = SimulationSettings(positions, 500, 3)
    settings_2 = SimulationSettings(positions, 500, 3)

    settings = [settings_1, settings_2]

    for setting in settings:

        service_settings = LocalizationServiceSettings(bounding_box, setting.particles, setting.transition_model_sigma, 20)

        view = TextView(NUMBER_OF_MEASUREMENTS)

        result_actor: List[BaseActor] = Result.start(view)

        observed_item: BaseActor = simulated_observed_item(setting.locators, result_actor)

        actors = LocalizationServiceActors(setting.locators_actors, [observed_item], [result_actor])

        localization_service: BaseActor = LocalizationService.start(actors, service_settings, NUMBER_OF_MEASUREMENTS)

        localization_service.tell(Start(localization_service))
    
        time.sleep(3)

        result_actor.stop()
        for locator in setting.locators_actors:
            locator.stop()
        observed_item.stop()
        localization_service.stop()
        
def send_start_message(localization_service: BaseActor):
    print("Sending Start message ...")
    localization_service.tell(Start(localization_service))

def simulated_observed_item(locators: List[Tuple[BaseActor, Point]],
                            result_actor: BaseActor) -> BaseActor:
    """Create and start the simulated observed item."""
    positions_to_play_x: List[int] = list(linspace(1, 180, NUMBER_OF_MEASUREMENTS))
    positions_to_play_y: List[int] = [10] * NUMBER_OF_MEASUREMENTS
    positions_to_play: List[Tuple[int, int]] = list(zip(positions_to_play_x, positions_to_play_y))

    return SimulatedObservedItem.start(locators, positions_to_play, result_actor)

def replay_observed_item(locators: List[Tuple[BaseActor, Point]],
                            result_actor: BaseActor) -> BaseActor:
    """Create and start the replay observed item."""
    positions_to_play_x: List[int] = list(linspace(0, 60, NUMBER_OF_MEASUREMENTS))
    positions_to_play_y: List[int] = [0] * NUMBER_OF_MEASUREMENTS
    positions_to_play: List[Tuple[int, int]] = list(zip(positions_to_play_x, positions_to_play_y))

    angles_to_play_1: List[float] = [-25, -27, -19, -12, -11, -12, -11, -8, -9, -13, -13, -13, -12, -11, -11, -9, -9, -5, 0, 4, 10, 15, 22, 27, 29, 33, 34, 36, 38, 40, 42, 44, 47, 51, 53, 54, 55, 56, 56, 57, 57, 58, 60, 61, 62, 63, 53, 55, 40, 45, 49, 52, 67, 70, 93, 81, 62, 67, 67, 72, 69, 85, 107, 96, 106, 108, 96, 91, 93, 102, 93, 87]
    angles_to_play_2: List[float] = [91, 74, 81, 83, 82, 85, 82, 83, 87, 79, 84, 75, 71, 66, 55, 54, 44, 46, 45, 50, 47, 50, 46, 44, 43, 43, 45, 45, 52, 53, 55, 55, 56, 56, 53, 53, 51, 50, 49, 48, 48, 47, 47, 46, 44, 43, 42, 40, 37, 35, 33, 32, 29, 25, 19, 13, 6, 0, -6, -11, -13, -13, -14, -14, -13, -12, -6, -10, -17, -8, 8, 1]
    angles_to_play: List[List[float]] = [angles_to_play_1, angles_to_play_2]

    return ReplayObservedItem.start(locators, positions_to_play, angles_to_play, result_actor)    

def replay_observed_item_2(locators: List[Tuple[BaseActor, Point]],
                            result_actor: BaseActor) -> BaseActor:
    """Create and start the replay observed item."""
    positions_to_play_x: List[int] = [29] * NUMBER_OF_MEASUREMENTS
    positions_to_play_y: List[int] = [0] * NUMBER_OF_MEASUREMENTS
    positions_to_play: List[Tuple[int, int]] = list(zip(positions_to_play_x, positions_to_play_y))

    angles_to_play_1: List[float] = [49, 49, 50, 50, 50, 51, 51, 51, 51, 50, 50, 50, 50, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 49, 50, 49, 49, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 49, 49, 49, 48, 48, 48, 48, 48, 48, 48, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 49, 50]
    angles_to_play_2: List[float] = [2, 2, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 2, 2, 2, 2, 1, 2, 2, 2, 3, 3, 4, 4, 4, 4, 4, 4, 4, 3, 4, 5, 5, 5, 6, 6, 5, 5, 6, 5, 5, 5, 6]
    angles_to_play: List[List[float]] = [angles_to_play_1, angles_to_play_2]

    return ReplayObservedItem.start(locators, positions_to_play, angles_to_play, result_actor)    

def replay_observed_item_3(locators: List[Tuple[BaseActor, Point]],
                            result_actor: BaseActor) -> BaseActor:
    """Create and start the replay observed item."""
    positions_to_play_x: List[int] = linspace(0, 58, NUMBER_OF_MEASUREMENTS)
    positions_to_play_y: List[int] = list(linspace(0, 21, int(NUMBER_OF_MEASUREMENTS / 2))) + list(linspace(21, 0, int(NUMBER_OF_MEASUREMENTS/2)))
    positions_to_play: List[Tuple[int, int]] = list(zip(positions_to_play_x, positions_to_play_y))

    angles_to_play_1: List[float] = [69, 2, -20, 8, 24, 14, -3, -2, 1, -20, -39, -43, -43, -32, -24, -14, -4, 13, 29, 40, 53, 64, 73, 79, 84, 88, 90, 94, 98, 103, 109, 112, 116, 118, 119, 118, 123, 120, 119, 114, 112, 111, 100, 98, 93, 90, 90, 89, 90, 95, 93, 97, 95, 90, 94, 87, 90, 92, 91, 93, 94, 96, 96, 97, 100, 105, 100, 99, 102, 99, 96]
    angles_to_play_2: List[float] = [-81, -57, -43, -35, -29, -28, -16, -8, -10, -16, -11, -9, -2, -3, -1, 7, 9, 19, 11, 14, 15, 15, 10, 2, 10, 9, 19, 20, 27, 32, 25, 17, 13, 15, 13, 13, 18, 24, 22, 15, 10, 4, -3, -9, -15, -19, -27, -39, -51, -64, -80, -93, -104, -111, -115, -116, -115, -116, -117, -115, -122, -128, -131, -128, -132, -140, -135, -132, -125, -132, -132]
    angles_to_play: List[List[float]] = [angles_to_play_1, angles_to_play_2]

    return ReplayObservedItem.start(locators, positions_to_play, angles_to_play, result_actor)    


class SimulationSettings():
    """Container for simulation settings."""

    def __init__(self, locators_positions: List[Point], particles: int, transition_model_sigma: float):
        self.locators_actors: List[BaseActor] = []
        for p in locators_positions:
            self.locators_actors.append(Locator.start(p))
        self.locators: List[Tuple[BaseActor, Point]] = list(zip(self.locators_actors, locators_positions))
        self.particles = particles
        self.transition_model_sigma = transition_model_sigma


if __name__ == "__main__":
    main()
