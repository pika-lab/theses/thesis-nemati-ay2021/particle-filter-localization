"""Localization service's actor and utilities."""

from enum import Enum
from typing import List, Optional

import numpy

import localization.messages as messages
from localization.base_actor import BaseActor
from localization.math_utils import Point, Rectangle, shortest_distance, line_of, intersection
from localization.particle_filtering import generate_uniform_particles
from localization.view import ViewInterface


class State(Enum):

    """The possible states of the Localization Service Actor."""

    Idle = 1
    Waiting = 2


class LocalizationServiceSettings():

    """A collection of all the settings needed for the localizaion service."""

    def __init__(self, bounding_box: Rectangle, starting_particles_number: int, sigma: float, seed: int = None) -> None:
        self.bounding_box = bounding_box
        self.starting_particles_number = starting_particles_number
        self.sigma = sigma
        self.seed = seed


class LocalizationServiceActors():

    """A collection of all the actors needed for the localization service."""

    def __init__(self,
                 locators: List[BaseActor],
                 observed_items: List[BaseActor],
                 result_actors: List[BaseActor]) -> None:
        self.locators = locators
        self.observed_items = observed_items
        self.result_actors: List[BaseActor] = result_actors


class ParticleFilteringData():

    """A collection of all the data needed for particle filtering."""

    def __init__(self) -> None:
        self.velocity: Point = (0, 0)
        self.center_of_mass: Point = (0, 0)


class LocalizationService(BaseActor):

    """The Actor impersonating the Localization Service."""

    def __init__(self,
                 actors: LocalizationServiceActors,
                 settings: LocalizationServiceSettings,
                 iterations: int) -> None:
        super().__init__({
            State.Idle: self.idle_on_receive,
            State.Waiting: self.waiting_on_receive
        }, State.Idle)
        self.actors = actors
        self.settings = settings
        self.angles: List[messages.SituatedAngle] = []
        self.points: List[Point] = []
        self.iterations: int = iterations
        self.particle_filtering = ParticleFilteringData()
        self.particle_filtering_setup()

    def particle_filtering_setup(self) -> None:
        """Setup of initial point cloud and ParticleFilteringData."""
        if self.settings.seed is not None:
            numpy.random.seed(self.settings.seed)
        self.points = generate_uniform_particles(self.settings.bounding_box[0][0],
                                                 self.settings.bounding_box[1][0],
                                                 self.settings.bounding_box[0][1],
                                                 self.settings.bounding_box[1][1],
                                                 self.settings.starting_particles_number)

    def prediction_step(self) -> List[Point]:
        """Perform prediction step through transition model."""
        new_points: List[Point] = []
        for p in self.points:
            new_x = numpy.random.normal(loc=p[0] + self.particle_filtering.velocity[0], scale=self.settings.sigma)
            new_y = numpy.random.normal(loc=p[1] + self.particle_filtering.velocity[1], scale=self.settings.sigma)
            new_points.append((new_x, new_y))
        return new_points

    def average_distances(self) -> List[float]:
        """Compute list of average distance for each point from each locator."""
        average_distances: List[float] = []
        for p in self.points:
            distances: List[float] = []
            for angle in self.angles:
                distances.append(shortest_distance(p, line_of(angle.position, angle.angle)))
            average_distances.append(sum(distances) / len(distances))

        return average_distances

    def center_of_mass(self) -> Optional[Point]:
        """Compute the center of mass of all the locators and angles."""
        intersections: List[Point] = []
        for angle in self.angles:
            for angle_2 in self.angles:
                if angle != angle_2:
                    intersection_point = intersection(
                            line_of(angle.position, angle.angle),
                            line_of(angle_2.position, angle_2.angle))
                    if intersection_point:
                        intersections.append(intersection_point)
        if len(intersections) > 0:
            average_x = sum(list(zip(*intersections))[0]) / len(intersections)
            average_y = sum(list(zip(*intersections))[1]) / len(intersections)
            return (average_x, average_y)
        return None

    def compute(self):
        """Perform particle filter step given previous state and new measurements."""

        # Prediction through transition model

        self.points = self.prediction_step()

        # Sensor Model

        # Find average distances for each point
        average_distances: List[float] = self.average_distances()

        # Compute weights
        max_average_distance = max(average_distances)
        weights: List[float] = []
        for average in average_distances:
            weights.append(max_average_distance / average)

        max_index = weights.index(max(weights))
        best_point = self.points[max_index]

        all_sum = sum(weights)
        normalized_weights = [w/all_sum for w in weights]

        self.resample(normalized_weights)

        # Update speed
        old_center_of_mass = self.particle_filtering.center_of_mass
        new_center_of_mass = self.center_of_mass()
        if new_center_of_mass:
            self.particle_filtering.center_of_mass = new_center_of_mass
            self.particle_filtering.velocity = (self.particle_filtering.center_of_mass[0] - old_center_of_mass[0],
                                                self.particle_filtering.center_of_mass[1] - old_center_of_mass[1])

        # Send points to Result actors
        for result_actor in self.actors.result_actors:
            result_actor.tell(messages.StateSnapshot(self.points))
            result_actor.tell(messages.BestPoint(best_point))

        self.angles = []

        self.iterations = self.iterations - 1
        if self.iterations > 0:
            message = messages.Step()
        else:
            message = messages.Stop()

        for locator in self.actors.locators:
            locator.tell(message)
        for observed_item in self.actors.observed_items:
            observed_item.tell(message)

    def resample(self, normalized_weights):
        """Perform resampling of the current points based on input weights."""
        points_x = []
        points_y = []
        for p in self.points:
            points_x.append(p[0])
            points_y.append(p[1])

        points_x = numpy.random.choice(points_x, len(self.points), p=normalized_weights)
        points_y = numpy.random.choice(points_y, len(self.points), p=normalized_weights)
        self.points = list(zip(points_x, points_y))

    def idle_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Idle."""
        if isinstance(message, messages.Start):
            for locator in self.actors.locators:
                locator.tell(message)
            for observed_item in self.actors.observed_items:
                observed_item.tell(message)
                observed_item.tell(messages.Step())
            self.state = State.Waiting

    def waiting_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Wait."""
        if isinstance(message, messages.SituatedAngle):
            self.angles.append(message)
            if len(self.angles) == (len(self.actors.locators) * len(self.actors.observed_items)):
                self.compute()
        if isinstance(message, messages.Stop):
            self.state = State.Idle


class Result(BaseActor):

    """An actor used for colleting the service's result."""

    def waiting_on_receive(self, message: messages.Message) -> None:
        """Handles messages when state is Waiting"""
        if isinstance(message, messages.StateSnapshot):
            self.computed_states.append(message.points)
            self.view.notify_new_data(message.points)
        if isinstance(message, messages.RealPosition):
            self.actual_positions.append(message.position)
            self.view.notify_real_position(message.position)
        if isinstance(message, messages.BestPoint):
            self.best_positions.append(message.position)
            self.view.notify_best_point(message.position)

    def __init__(self, view: ViewInterface) -> None:
        super().__init__({
            State.Waiting: self.waiting_on_receive
        }, State.Waiting)
        self.view: ViewInterface = view
        self.computed_states: List[List[Point]] = []
        self.actual_positions: List[Point] = []
        self.best_positions: List[Point] = []
