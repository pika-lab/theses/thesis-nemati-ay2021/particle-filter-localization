"""Locator's actor and utilities."""

from enum import Enum
import logging

import localization.messages as messages
from localization.base_actor import BaseActor
from localization.math_utils import Point


class State(Enum):

    """The possible states of the Locator Actor."""

    Idle = 1
    ReadAngle = 2
    Waiting = 3


class Locator(BaseActor):

    """The Actor impersonating the Locator."""

    def __init__(self, position: Point) -> None:
        super().__init__({
            State.Idle: self.idle_on_receive,
            State.ReadAngle: self.read_angle_on_receive,
            State.Waiting: self.waiting_on_receive
        }, State.Idle)
        self.position: Point = position
        self.localization_service: BaseActor

    def idle_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Idle."""
        if isinstance(message, messages.Start):
            self.localization_service = message.localization_service
            self.state = State.ReadAngle

    def read_angle_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Read Angle."""
        if isinstance(message, messages.Angle):
            customized_message = messages.SituatedAngle(message.angle, self.position)
            self.localization_service.tell(customized_message)
            self.state = State.Waiting

    def waiting_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Waiting."""
        if isinstance(message, messages.Step):
            self.state = State.ReadAngle
        if isinstance(message, messages.Stop):
            logging.info("Locator stopped")
            self.state = State.Idle
