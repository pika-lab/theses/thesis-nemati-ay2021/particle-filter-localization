"""Module containing Particle Filtering utilities and methods."""

from typing import Iterable, Tuple, List
from math import sqrt

from numpy import linspace

from localization.math_utils import Point


def generate_uniform_particles(starting_x: float,
                               ending_x: float,
                               starting_y: float,
                               ending_y: float,
                               n: int) -> List[Tuple[float, float]]:
    """Generate equally distantiated particles in the defined space."""
    if n <= 0:
        raise ValueError

    particles: List[Point] = []

    x_range: Iterable[float] = linspace(starting_x, ending_x, int(sqrt(n)))
    y_range: Iterable[float] = linspace(starting_y, ending_y, int(sqrt(n)))

    for x in x_range:
        for y in y_range:
            particles.append((x, y))

    return particles
