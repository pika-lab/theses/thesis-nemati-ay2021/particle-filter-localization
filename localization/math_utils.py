"""Some mathematical utilities useful for the system."""

import math
from typing import Tuple, Optional

Point = Tuple[float, float]  # (x, y)


Line = Tuple[float, float, float]  # ax + by + c = 0


Rectangle = Tuple[Point, Point]  # (bottom-left-corner, upper-right-corner)


def radians_to_degrees(angle_radians: float) -> float:
    """Transform the given radians angle in degrees."""
    return angle_radians * 180 / math.pi


def angle_of(p_1: Point, p_2: Point) -> float:
    """Compute the clockwise angle between two points."""
    delta_x = p_1[0] - p_2[0]
    delta_y = p_1[1] - p_2[1]
    angle_radians = math.atan2(delta_y, delta_x)
    angle_degrees = radians_to_degrees(angle_radians)
    if angle_degrees >= 0:
        return angle_degrees
    return 180 + angle_degrees


def shortest_distance(p: Point, line: Line) -> float:
    """Shortest distance between line and point."""
    x, y = p
    a, b, c = line

    return math.fabs(a * x + b * y + c) / math.sqrt(a * a + b * b)


def line_of(p: Point, alpha: float) -> Line:
    """Compute the line given a point and an angle."""
    m = math.tan(math.radians(alpha))
    q = p[1] - m * p[0]
    a = -1 * m
    b = 1
    c = -1 * q
    return (a, b, c)


def intersection(line_1: Line, line_2: Line) -> Optional[Point]:
    """Compute the intersection point of the two given lines."""
    a, b, c = line_1
    a_2, b_2, c_2 = line_2
    if a == a_2 and b == b_2:
        return None
    if a == 0 and a_2 != 0:
        y = - (c / b)
        x = (- (b_2 * y) - c_2) / a_2
    elif a_2 == 0 and a != 0:
        y = -c_2 / b_2
        x = - (b * y - c) / a
    elif b_2 == 0 and a_2 != 0:
        x = - c_2 / a_2
        y = - (a * x + c) / b
    else:
        x = (((b * c_2) / (b_2 * a)) - c / a) / (1 - ((b * a_2) / (a * b_2)))
        y = (-a_2 * b_2) * x - c_2 / b_2
    return x, y
