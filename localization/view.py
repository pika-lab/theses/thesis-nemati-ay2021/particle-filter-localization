"""View for displaying the point cloud."""

import math
import tkinter
from typing import List, Any, Tuple

from localization.math_utils import Point


class ViewInterface():
    """Interface for every View."""

    def notify_new_data(self, points: List[Point]) -> None:
        """Notify the view that new data is available and pass it."""

    def notify_real_position(self, p: Point) -> None:
        """Notify the view about the real position of the oberved item."""

    def notify_best_point(self, p: Point) -> None:
        """Notify the view about the point that better approximates the real position."""


class TkinterView(ViewInterface):
    """View implementation using Tkinter library."""

    def __init__(self, root, width: int, height: int) -> None:
        self.history = PointsHistory()
        self.root = root
        self.canvas = tkinter.Canvas(root, width=width, height=height)
        self.accuracy_text = tkinter.Label(self.root, text="Accuracy")
        self.ovals: List[Any] = []
        self.last_drawn = 0
        self.offset = (50, 50)

    def notify_new_data(self, points: List[Point]) -> None:
        """Notify the view that new data is available and pass it."""
        self.history.points.append(points)

    def notify_real_position(self, p: Point) -> None:
        """Notify the view about the real position of the oberved item."""
        self.history.real_positions.append(p)

    def notify_best_point(self, p: Point) -> None:
        """Notify the view about the point that better approximates the real position."""
        self.history.best_points.append(p)

    def start(self) -> None:
        """Display the Tkinter window."""
        self.canvas.pack()
        self.root.after(100, self.repaint)
        self.accuracy_text.pack()
        tkinter.mainloop()

    def repaint(self) -> None:
        """Paint next step."""
        if len(self.history.points) > self.last_drawn:
            points = self.history.points[self.last_drawn]
            for oval in self.ovals:
                self.canvas.delete(oval)
            self.ovals = []
            for p in points:
                self.ovals.append(self.draw_oval(p, 3, (self.offset[0], self.offset[1])))
            p = self.history.real_positions[self.last_drawn]
            self.draw_oval(p, 4, (self.offset[0], self.offset[1]), "red")
            q = self.history.best_points[self.last_drawn]
            self.draw_oval(q, 2, (self.offset[0], self.offset[1]), "blue")
            distance = math.sqrt((p[0] - q[0])**2 + (p[1] - q[1])**2)
            self.history.accuracy.append(distance)
            self.accuracy_text['text'] = "Accuracy: " + str(distance) + \
                " | mean accuracy: " + str(sum(self.history.accuracy) / len(self.history.accuracy))
            self.last_drawn = self.last_drawn + 1
        self.root.after(100, self.repaint)

    def draw_oval(self, p: Point, ray: int, offset: Tuple[int, int], fill: str = "black") -> Any:
        """Draw oval at the current position and return itsresult."""
        return self.canvas.create_oval(p[0] - ray + offset[0],
                                       p[1] - ray + offset[1],
                                       p[0] + ray + offset[0],
                                       p[1] + ray + offset[1],
                                       fill=fill)


class PointsHistory():
    """Container for history of points."""

    def __init__(self):
        self.points: List[List[Point]] = []
        self.real_positions: List[Point] = []
        self.best_points: List[Point] = []
        self.accuracy: List[float] = []


class TextView(ViewInterface):
    """View implementation based on text."""

    def __init__(self, iterations: int):
        self.history = PointsHistory()
        self.iterations = iterations

    def notify_new_data(self, points: List[Point]) -> None:
        """Notify the view that new data is available and pass it."""
        self.history.points.append(points)

    def notify_real_position(self, p: Point) -> None:
        """Notify the view about the real position of the oberved item."""
        self.history.real_positions.append(p)

    def notify_best_point(self, p: Point) -> None:
        """Notify the view about the point that better approximates the real position."""
        q = self.history.real_positions[len(self.history.best_points)]
        self.history.best_points.append(p)
        distance = math.sqrt((p[0] - q[0])**2 + (p[1] - q[1])**2)
        self.history.accuracy.append(distance)
        if len(self.history.best_points) == self.iterations:
            print(sum(self.history.accuracy) / len(self.history.accuracy))
