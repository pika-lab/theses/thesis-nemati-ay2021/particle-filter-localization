"""Actor utilities used by all actors in the system."""

from typing import Any, Callable, Dict, List, TypeVar, Generic

import pykka

import localization.messages as messages

State = TypeVar('State')


class BaseActor(pykka.ThreadingActor, Generic[State]):

    """The Base Actor to inherit from."""

    def __init__(self,
                 message_handlers: Dict[State, Callable[[messages.Message], Any]],
                 initial_state: State) -> None:
        super().__init__()
        self.state = initial_state
        self.message_handlers = message_handlers
        self.stored_messages: List[messages.Message] = []

    def on_receive(self, message: messages.Message) -> Any:
        handler = self.message_handlers[self.state]
        return handler(message)
