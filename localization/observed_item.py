"""Observed Item's actor and utilities."""

from enum import Enum
import abc
from typing import List, Tuple
import logging

import numpy

import localization.messages as messages
from localization.base_actor import BaseActor
from localization.math_utils import Point, angle_of


class State(Enum):

    """The possible states of the Locator Actor."""

    Idle = 1
    Waiting = 2


class ObservedItem(BaseActor):

    """The Actor impersonating the Observed Item."""

    @abc.abstractmethod
    def compute_angle_message(self, locator: BaseActor) -> messages.Angle:
        """Calculate the angle to that locator and return the Angle message."""

    def idle_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Idle."""
        if isinstance(message, messages.Start):
            self.state = State.Waiting

    def waiting_on_receive(self, message: messages.Message) -> None:
        """Handle messages when state is Waiting."""
        if isinstance(message, messages.Step):
            for locator in self.locators:
                locator.tell(self.compute_angle_message(locator))
        if isinstance(message, messages.Stop):
            self.state = State.Idle

    def __init__(self, locators: List[BaseActor]) -> None:
        super().__init__({
            State.Idle: self.idle_on_receive,
            State.Waiting: self.waiting_on_receive
        }, State.Idle)
        self.locators = locators


class SimulatedObservedItem(ObservedItem):

    """A simulated version of the ObservedItem."""

    def __init__(self, locators: List[Tuple[BaseActor, Point]],
                 positions_to_play: List[Point],
                 result_actor: BaseActor,
                 sigma: float = 0.1) -> None:
        locators_actors, locators_positions = zip(*locators)
        super().__init__(locators_actors)
        self.locators_positions = locators_positions
        self.positions_to_play = positions_to_play
        logging.info(positions_to_play)
        self.current_point: Point = positions_to_play.pop(0)
        self.locators_remaining_in_step: int = len(self.locators)
        self.result_actor = result_actor
        self.sigma = sigma

    def compute_angle(self, locator: BaseActor) -> float:
        """Compute the angle to send."""
        locator_position = self.locators_positions[self.locators.index(locator)]
        point_with_noise = (numpy.random.normal(loc=self.current_point[0], scale=self.sigma),
                            numpy.random.normal(loc=self.current_point[1], scale=self.sigma))
        return angle_of(point_with_noise, locator_position)

    def compute_angle_message(self, locator: BaseActor) -> messages.Angle:
        """Calculate the angle to that locator and return the Angle message."""
        angle = self.compute_angle(locator)

        if self.locators_remaining_in_step == 1:
            self.locators_remaining_in_step = len(self.locators)
            self.result_actor.tell(messages.RealPosition(self.current_point))
            if len(self.positions_to_play) > 0:
                self.current_point = self.positions_to_play.pop(0)
        else:
            self.locators_remaining_in_step = self.locators_remaining_in_step - 1

        return messages.Angle(angle)


class ReplayObservedItem(SimulatedObservedItem):

    """A simulated version of the ObservedItem the plays previously recorded data."""

    def __init__(self, locators: List[Tuple[BaseActor, Point]],
                 positions_to_play: List[Point],
                 angles_to_play: List[List[float]],
                 result_actor: BaseActor) -> None:
        super().__init__(locators, positions_to_play, result_actor)
        self.angles_to_play = angles_to_play

    def compute_angle(self, locator: BaseActor) -> float:
        """Compute the angle to send."""
        index = self.locators.index(locator)
        return self.angles_to_play[index].pop()
