"""Contains custom exceptions used in the system."""


class SimulationOverrunnException(Exception):
    """Exception raised when the simulation data is finished but  a next step is required."""
