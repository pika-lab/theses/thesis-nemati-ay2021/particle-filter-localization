"""Messages of the Actor system."""

from typing import List
from pykka import ThreadingActor

from localization.math_utils import Point


class Message:
    """Base class for all messages to inherit from."""


class Start(Message):
    """Message for transitioning from Idle state to some working state."""
    def __init__(self, localization_service: ThreadingActor) -> None:
        self.localization_service = localization_service


class Step(Message):
    """Message for indicating that the next step can be done."""


class Stop(Message):
    """Message for stopping the current work."""


class Angle(Message):

    """Message for communicating the registered angle."""

    def __init__(self, angle: float) -> None:
        self.angle = angle


class SituatedAngle(Message):

    """Message for communicating an angle and a position."""

    def __init__(self, angle: float, position: Point) -> None:
        self.angle = angle
        self.position = position


class StateSnapshot(Message):

    """Message for communicating the current state."""

    def __init__(self, points: List[Point]) -> None:
        self.points = points


class RealPosition(Message):

    """Message for communicating the real position of the Observed item in a simulation."""

    def __init__(self, position: Point) -> None:
        self.position = position


class BestPoint(Message):

    """Message for communicating the position with the highest probability of being the real position."""

    def __init__(self, position: Point) -> None:
        self.position = position
