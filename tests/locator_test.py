import unittest.mock

import pytest

from localization.locator import Locator
from localization.messages import Start, Step, Angle


@pytest.fixture
def localization_service_mock():
    yield unittest.mock.Mock()

@pytest.fixture
def locator(localization_service_mock):
    locator = Locator.start(localization_service_mock)
    yield locator
    locator.stop()

def test_locator_actor(locator, localization_service_mock):
    locator.ask(Start(localization_service_mock))
    locator.ask(Angle(1))
    localization_service_mock.tell.assert_called()