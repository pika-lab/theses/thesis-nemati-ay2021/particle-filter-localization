import unittest.mock

import pytest

from localization.observed_item import ObservedItem
from localization.messages import Start, Step


@pytest.fixture
def locator_mock():
    yield unittest.mock.Mock()

@pytest.fixture
def observed_item(locator_mock):
    observed_item = ObservedItem.start([locator_mock])
    yield observed_item
    observed_item.stop()

@pytest.fixture
def localization_service_mock():
    yield unittest.mock.Mock()

def test_observed_item_actor(observed_item, locator_mock, localization_service_mock):
    observed_item.ask(Start(localization_service_mock))
    observed_item.ask(Step())
    locator_mock.tell.assert_called()
