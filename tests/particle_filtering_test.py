import unittest.mock

import pytest

from localization.particle_filtering import generate_uniform_particles

@pytest.fixture
def expected_particles():
    return [(1, 1), (1, 2), (1, 3),
            (2, 1), (2, 2), (2, 3),
            (3, 1), (3, 2), (3, 3)]

def generate_uniform_particles_test():
    assert generate_uniform_particles(1, 3, 1, 3, 9) == expected_particles