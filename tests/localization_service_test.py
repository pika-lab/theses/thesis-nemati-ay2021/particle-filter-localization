import unittest.mock

import pytest

from localization.localization_service import LocalizationService, LocalizationServiceSettings, LocalizationServiceActors
from localization.messages import Start, Step


@pytest.fixture
def locator_mock():
    yield unittest.mock.Mock()

@pytest.fixture
def observed_item_mock():
    yield unittest.mock.Mock()

@pytest.fixture
def result_mock():
    yield unittest.mock.Mock()

@pytest.fixture
def actors(locator_mock, observed_item_mock, result_mock):
    return LocalizationServiceActors([locator_mock], [observed_item_mock], [result_mock])

@pytest.fixture
def settings():
    return LocalizationServiceSettings(((0, 0), (1, 1)), 1, 1)

@pytest.fixture
def localization_service(actors, settings):
    localization_service = LocalizationService.start(actors, settings, 1)
    yield localization_service
    localization_service.stop()

def test_localization_service_actor(localization_service, locator_mock, observed_item_mock):
    localization_service.ask(Start(localization_service))
    observed_item_mock.tell.assert_called()
    locator_mock.tell.assert_called()