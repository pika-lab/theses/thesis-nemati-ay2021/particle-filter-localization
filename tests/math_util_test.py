import pytest

from localization.math_utils import angle_of, shortest_distance, line_of, intersection

@pytest.fixture
def p_1_1():
    yield (1,1)

@pytest.fixture
def p_2_2():
    yield (2,2)

def test_angle_of(p_1_1, p_2_2):
    assert angle_of(p_1_1, p_2_2) == 45

def test_angle_of_commutative(p_1_1, p_2_2):
    assert angle_of(p_1_1, p_2_2) == angle_of(p_2_2, p_1_1)

def test_shortest_distance():
    assert pytest.approx((shortest_distance((2, 0), (1, -1, 0))), 0.0001) == 1.4142

def test_line_of(p_1_1):
    (a, b, c) = line_of(p_1_1, 45)
    assert pytest.approx((a, b, c), 0.0001) == (-1, 1, 0)

def test_intersection(p_1_1):
    l1 = line_of(p_1_1, 45)
    l2 = line_of(p_1_1, -45)
    assert pytest.approx(intersection(l1, l2), 0.001) == p_1_1

def test_intersection_2(p_1_1):
    l1 = line_of((0, 0), 45)
    l2 = line_of((2, 0), -45)
    assert pytest.approx(intersection(l1, l2), 0.001) == p_1_1

def test_parallel_intersection():
    l1 = line_of((0, 0), 45)
    l2 = line_of((1, 0), 45)
    assert intersection(l1, l2) == None